FROM ubuntu:20.04
MAINTAINER Simon Hardt <info@nullcode.de>

# latest command-line-tools for linux from https://developer.android.com/studio/index.html#downloads
ENV VERSION_TOOLS "6858069"

ENV VERSION_BUILD_TOOLS "30.0.2"
ENV VERSION_TARGET_SDK "30"
ENV VERSION_NDK "21.1.6352462"

# --- do not change lines below ---

ENV ANDROID_SDK_ROOT "/sdk"
# Keep alias for compatibility
ENV ANDROID_HOME "${ANDROID_SDK_ROOT}"

# Append the directory `$ANDROID_SDK_ROOT/cmdline-tools/tools/bin` to environment variable PATH, so that the system knows where to find sdkmanager
ENV PATH "${PATH}:${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin"

# Prepare System
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -qq update \
 && apt-get install -qqy --no-install-recommends \
      curl \
      html2text \
      openjdk-11-jdk \
      libc6-i386 \
      lib32stdc++6 \
      lib32gcc1 \
      lib32ncurses6 \
      lib32z1 \
      unzip \
      locales \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN locale-gen en_US.UTF-8
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

#   # GitLab injects the username as ENV-variable which will crash a gradle-build.
#   # Workaround by adding unicode-support.
#   # See
#   # https://github.com/gradle/gradle/issues/3117#issuecomment-336192694
#   # https://github.com/tianon/docker-brew-debian/issues/45
#   RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
#   ENV LANG en_US.UTF-8

RUN rm -f /etc/ssl/certs/java/cacerts; \
    /var/lib/dpkg/info/ca-certificates-java.postinst configure

# Download "Android SDK Command-line Tools" and extract it into a directory called `cmdline-tools`, which is inside `ANDROID_SDK_ROOT`
# https://stackoverflow.com/a/61176718/2170109
RUN curl -s https://dl.google.com/android/repository/commandlinetools-linux-${VERSION_TOOLS}_latest.zip > /tools.zip \
 && mkdir -p ${ANDROID_SDK_ROOT}/cmdline-tools \
 && unzip /tools.zip -d ${ANDROID_SDK_ROOT}/cmdline-tools \
 && mv ${ANDROID_SDK_ROOT}/cmdline-tools/cmdline-tools ${ANDROID_SDK_ROOT}/cmdline-tools/tools \
 && rm -v /tools.zip

# Accept Licenses
# https://stackoverflow.com/a/45782695/2170109
RUN yes | sdkmanager --licenses

# Install important SDK Packages
RUN sdkmanager "platforms;android-${VERSION_TARGET_SDK}" \
 && sdkmanager "build-tools;${VERSION_BUILD_TOOLS}" \
 && sdkmanager "platform-tools" \
 && sdkmanager "ndk;${VERSION_NDK}"

#   # Install every SDK package
#   RUN while read -r package; do PACKAGES="${PACKAGES}${package} "; done < /sdk/packages.txt \
#    && sdkmanager --sdk_root=${ANDROID_SDK_ROOT} ${PACKAGES}
